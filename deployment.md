# Deployment methods

## AWS

```
[Unit]
Description=Application
After=network-online.target

[Service]
Restart=on-failure
WorkingDirectory=/home/ubuntu/app/
ExecStart=/usr/bin/node /home/ubuntu/app/server.js
Environment=NODE_ENVIRONMENT=Test
EnvironmentFile=/home/ubuntu/app/env

[Install]
WantedBy=multi-user.target
```

File location: /etc/systemd/system/
Commands:
```
systemctl enable website-name.service
systemctl daemon-reload
```
