require('dotenv').config();

const express = require('express');
const compression = require('compression');
const exphbs = require('express-handlebars');
const path = require('path');
const manifest = require('./build/manifest.json');

const app = express();

const assetOptions = {
  setHeaders: (res) => {
    res.set('Service-Worker-Allowed', '/');
    res.set('Cache-Control', 'public, max-age=31536000');
  },
};

app.use(compression());

app.engine('.hbs', exphbs({
  defaultLayout: 'main',
  extname: '.hbs',
  layoutsDir: 'src/views/layouts',
  partialsDir: 'src/views/partials',
}));

app.set('view engine', '.hbs');
app.set('views', 'src/views');

// Serving static files
app.use('/build', express.static(path.resolve(__dirname, 'build'), assetOptions));
app.use('/media', express.static(path.resolve(__dirname, 'media')));


// hide powered by express
app.disable('x-powered-by');
// start the server
app.listen(process.env.PORT);

// SSR function import
const ssr = require('./views/server');

function generateTemplateData(initialState, styles, content, title) {
  return {
    windowStateScript: `<script>window.STATE = ${JSON.stringify(initialState)};</script>`,
    scripts: Object.keys(manifest).filter(key => key === 'client.js').map(key => manifest[key]),
    stylesheets: Object.keys(manifest).filter(key => key === 'client.css').map(key => manifest[key]),
    fonts: Object.keys(manifest).filter(key => key.includes('fonts')).map(key => manifest[key]),
    styles,
    title,
    content,
  };
}

// server rendered home page
app.get('/', (req, res) => {
  const initialState = { text: 'Hurricane X project' };
  const { content, styles } = ssr(initialState);

  const data = generateTemplateData(initialState, styles, content, 'Hurricane X project -- Homepage');

  res.setHeader('Cache-Control', 'build, max-age=604800');
  res.render('page', data);
});

app.get('/offline', (req, res) => {
  const initialState = { text: 'Hurricane X project -- Offline' };
  const { content, styles } = ssr(initialState);

  const data = generateTemplateData(initialState, styles, content, 'Hurricane X project -- Offline');

  res.setHeader('Cache-Control', 'build, max-age=604800');
  res.render('page', data);
});
