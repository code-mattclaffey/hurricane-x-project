# The Hurricane X Project - SSR React Boilerplate

> An ExpressJS Server that uses React

## Features

- Server Side rendering
- Styled components
- CSS Modules
- Storybook
- Offline caching of fonts, css & scripts
- Offline Page
- Caching Strategies
- ESLint & StyleLint
- Polyfill.io for Polyfilling the bundles
- Image Optimistion
- Cypress testing with Accesibility WCAG 2.0 testing validation on pages tested
- Setup in minutes
- Bridges the gap between Design, FE & BE. 
- Jest & enzyme testing framework

## Setup

First npm install all of the things...

Then setup a `.env` file with the following:

```
NODE_ENV=development
PORT=3000
BUILD_NUMBER=local
CYPRESS_URL=http://localhost:3000
```

Once created run `npm run start`. You will find your app running on `localhost:3000`.

## Cypress tooling

Cypress is an acceptance testing tool that is run via JavaScript. We have installed a11y tests which will check for accessibility on the page. It will fail the build if the page has errors. 

> Pro tip: make a styleguide page which will check for general a11y issues on the components.

The commands to run cypress are:
- `npm run cypress:setup` - use this for the first time to get `CYPRESS_URL` from your .env file inserted into the cypress env.
- `npm run cypress:open` - runs the UI on your local env so you can visually see the tests running. Good for running one set of tests instead of all at once
- `npm run cypress:ci` - bulk runs all the cypress spec file tests in the terminal

Currently Cypress supports WCAG 2.0 but is looking to support 2.1 soon. [See article about WCAG 2.1](https://www.deque.com/blog/support-for-wcag-2-1-in-axe-core/)


## Deployment

Make sure that the enviroment variables are added in your build pipelines. Here is a list of the important ones you will need:

- **PORT** - may only be needed on local but depends on where you deploy your app to. Heroku & Firebase have predefined ports when we deploy our app.
- **NODE_ENV** - used to build our production scripts & styles. Values are either `development` or `production`
- **BUILD_NUMBER** - currently only used for the service worker but it could be used for other things if needed.