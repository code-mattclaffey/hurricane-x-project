const fs = require('fs');
require('dotenv').config();

function buildConfig(CYPRESS_URL) {
  return `{
    "CYPRESS_URL": "${CYPRESS_URL}"
  }`;
}

function CreateENVFile() {
  return new Promise((res, rej) => {
    const string = buildConfig(process.env.CYPRESS_URL);
    // eslint-disable-next-line consistent-return
    fs.writeFile('cypress.env.json', string, (err) => {
      if (err) return rej(err);
      res();
    });
  })
    .catch((error) => {
      throw error;
    });
}

CreateENVFile();
