require('dotenv').config();

const Handlebars = require('handlebars');
const fs = require('fs');
const manifest = require('../build/manifest.json');

function getSource(src) {
  return new Promise((resolve, reject) => {
    fs.readFile(src, 'utf-8', (err, contents) => {
      if (err) reject(err);
      resolve(contents);
    });
  });
}

function writetoDist(dist, result) {
  return new Promise((resolve, reject) => {
    fs.writeFile(dist, result, (err) => {
      if (err) reject(err);
      console.log('Successfully written service worker');
      resolve();
    });
  });
}

async function BuildServiceWorker() {
  const source = await getSource('./src/assets/service-worker.tpl');

  const template = Handlebars.compile(source);

  const data = {
    BUILD_NUMBER: process.env.CI_BUILD_ID === 'local' ? `${process.env.CI_BUILD_ID}-${Math.floor(Math.random() * 1000) + 1}` : process.env.CI_BUILD_ID,
    FILES: Object.keys(manifest).map(key => manifest[key]),
  };

  const result = template(data);

  await writetoDist('./build/service-worker.js', result);
}

BuildServiceWorker();
