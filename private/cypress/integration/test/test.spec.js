context('Actions', () => {
  beforeEach(() => {
    cy.visit(Cypress.env('CYPRESS_URL'));
    cy.injectAxe();
  });

  it('should check for all a11y errors on the page', () => {
    cy.checkA11y();
  });
});
