require('dotenv').config();

const path = require('path');
const ManifestPlugin = require('webpack-manifest-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;

const patterns = [
  { from: './src/assets/pwa-manifest.json', to: './' },
  { from: './src/assets/favicon.ico', to: './' },
  { from: './src/assets/images/' },
];

const isProduction = process.env.NODE_ENV === 'prod';

const postcssconfig = [
  autoprefixer({
    browsers: [
      '>1%',
      'last 4 versions',
      'Firefox ESR',
      'not ie < 9', // React doesn't support IE8 anyway
    ],
    flexbox: 'no-2009',
  }),
];

const plugins = [
  new ManifestPlugin(),
  new CopyWebpackPlugin([...patterns]),
  new MiniCssExtractPlugin({
    // Options similar to the same options in webpackOptions.output
    // both options are optional
    filename: '[name].[chunkhash].css',
  }),
  new ImageminPlugin(
    {
      test: /\.(jpe?g|png|gif|svg)$/i,
      jpegtran: {
        quality: 70,
        progressive: true,
      },
    },
  ),
  new StyleLintPlugin(),
];

if (isProduction) postcssconfig.push(cssnano());

module.exports = {
  mode: isProduction ? 'production' : 'development',
  entry: {
    client: './src/client.js',
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].[chunkhash].js',
  },
  module: {
    rules: [
      {
        test: /\.jsx?/i,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/env', '@babel/react'],
              plugins: [],
            },
          },
          'eslint-loader',
        ],
      },
      {
        test: /\.(css|scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: () => postcssconfig,
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.(woff(2)?)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[hash].[ext]',
            useRelativePath: true,
            outputPath: 'fonts/',
          },
        }],
      },
    ],
  },
  plugins,
};
