import React from 'react';
import { renderToString } from 'react-dom/server';
import { ServerStyleSheet } from 'styled-components';
import App from './components/app';

module.exports = function render(state) {
  // Model the initial state
  const sheet = new ServerStyleSheet();
  const content = renderToString(
    sheet.collectStyles(<App state={state} />),
  );

  const styles = sheet.getStyleTags();
  return { content, styles };
};
