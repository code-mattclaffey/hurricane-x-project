
/*

  @author Matt Claffey

*/

var cacheFiles = [
  '/offline'
];

var staticCacheFiles = [
  {{#each FILES}}
    '/build/{{this}}',
  {{/each}}
];

// if assets live on another domain
var staticDomain = '';

var cacheName = 'version-{{BUILD_NUMBER}}';

self.addEventListener('install', function(event) {

	event.waitUntil(
    caches.open(cacheName)
      .then(function (cache) {
        const cacheUrls = cacheFiles.concat(staticCacheFiles.map(url => `${staticDomain}${url}`));
        return cache.addAll(cacheUrls);
      })
      .then(function() {
        return self.skipWaiting();
      })
			.catch(function(error) {
        console.error(error);
      })
	);
});


// Empty out any caches that donâ€™t match the ones listed.
self.addEventListener('activate', function(event) {
  var cacheWhitelist = [cacheName];

  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.map(function(cacheName) {
          if (cacheWhitelist.indexOf(cacheName) === -1) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  );

  return self.clients.claim();
});

self.addEventListener('fetch', function(event) {
  const requestUrl = event.request.url.replace(/^.*\/\/[^\/]+/, '');
  const allCacheFiles = cacheFiles.concat(staticCacheFiles);

  if (allCacheFiles.includes(requestUrl)) {
    event.respondWith(caches.match(event.request)
      .then(cacheResponse => cacheResponse || fetch(event.request)));
  }

  if (event.request.mode === 'navigate' ||
      (event.request.method === 'GET' &&
      event.request.headers.get('accept').includes('text/html'))) {
    event.respondWith(
      fetch(event.request).catch(error => {
        return caches.match('/offline');
      })
    );
  }
});
