import styled, { css } from 'styled-components';

export const VisibilityHidden = styled.span`
  border: 0;
  clip: rect(0, 0, 0, 0);
  height: 1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
  word-wrap: normal;
`;

export default {};
