import React from 'react';
import styled from 'styled-components';

const SVG = styled.svg`
  display: inline-block;
  height: 200px;
  width: 200px;
`;

class Logo extends React.Component {
  constructor() {
    super();

    this.state = {
      strokeFill: '#ffffff',
    };
  }

  componentDidMount() {
    const { state } = this;
    const { props } = this;

    const newState = { ...state, ...props };

    this.setState({ ...newState });
  }

  render() {
    const { strokeFill } = this.state;

    return (
      <SVG xmlns="http://www.w3.org/2000/svg" viewBox="-11.5 -10.23174 23 20.46348">
        <title>React Logo</title>
        <circle cx="0" cy="0" r="2.05" fill={strokeFill} />
        <g stroke={strokeFill} strokeWidth="1" fill="none">
          <ellipse rx="11" ry="4.2" />
          <ellipse rx="11" ry="4.2" transform="rotate(60)" />
          <ellipse rx="11" ry="4.2" transform="rotate(120)" />
        </g>
      </SVG>
    );
  }
}

export default Logo;
