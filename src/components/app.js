import React from 'react';
import styled, { ThemeProvider } from 'styled-components';
import Logo from './_atoms/logo/index';
import theme from './theme';

// Our single Styled Component definition
const AppContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  width: 100%;
  height: 100%;
  font-size: 40px;
  background: ${props => props.theme.colours.grad};
`;

const Heading = styled.h1`
  color: white;
  font-size: 3rem;
  max-width: 960px;
  text-align: center;
  width: 100%;
`;

const HeadingText = styled.span`
  display: block;
`;

class App extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { state } = this.props;
    const { text } = state;
    return (
      <ThemeProvider theme={theme}>
        <AppContainer>
          <Heading>
            <Logo />
            <HeadingText>{text}</HeadingText>
          </Heading>
        </AppContainer>
      </ThemeProvider>
    );
  }
}

export default App;
