import React from 'react';
import ReactDOM from 'react-dom';
import 'lazysizes';
import App from './components/app';

import './styles/main.scss';
import './assets/fonts/roboto-black-webfont.woff2';
import './assets/fonts/roboto-light-webfont.woff2';
import './assets/fonts/roboto-regular-webfont.woff2';

const state = window.STATE;
delete window.STATE;

ReactDOM.render(
  <App state={state} />,
  document.querySelector('#app'),
);
